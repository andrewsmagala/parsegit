package parsegit

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"os/exec"
	"strings"
)

type Parser struct {
	Dir    string
	After  string
	Commit CommitType
}

type valChange struct {
	Key string `json:"Key"`
	Val string `json:"Val"`
}

func (v *valChange) String() string {
	b, err := json.Marshal(v)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

type diff struct {
	File    string      `json:"File"`
	Changes []valChange `json:"Changes"`
}

func (d *diff) String() string {
	b, err := json.Marshal(d)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

type CommitType struct {
	Author  string `xml:"author"`
	Date    string `xml:"date"`
	Message string `xml:"message"`
	Hash    string `xml:"hash"`
	Diffs   []diff `json:"Diffs"`
}

func (c *CommitType) DiffString() string {
	var stringList []string
	for _, diff := range c.Diffs {
		stringList = append(stringList, diff.String())
	}
	retString := strings.Join(stringList, ",")
	retString = "[" + retString + "]"
	return retString
}

func NewParser(dir, after string) (*Parser, error) {
	p := &Parser{
		Dir:   dir,
		After: after,
	}
	return p, nil
}

func (p *Parser) GetCommits() error {
	format := "<commit><author>%an</author><date>%cd</date><message>%B</message><hash>%H</hash></commit>"
	var cmd *exec.Cmd
	cmd = exec.Command("git", "log", "-1", "--pretty=format:"+format, "--after="+p.After)
	cmd.Dir = p.Dir
	out, err := cmd.Output()
	if err != nil {
		return err
	}
	tmpString := string(out)
	var commit *CommitType
	xml.Unmarshal([]byte(tmpString), &commit)
	p.Commit = *commit
	return nil
}

func (p *Parser) GetDiffs(commitHash string) (string, error) {
	cmd := exec.Command("git", "diff", commitHash+"~", commitHash)
	cmd.Dir = strings.ReplaceAll(p.Dir, "\\.git", "")
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}
	return bytes.NewBuffer(out).String(), nil
}

func (p *Parser) ParseDiffs(c *CommitType, diffString string) error {
	diffStringArray := strings.Split(diffString, "+++ ")
	var diffStruct diff
	var valueChange valChange
	for i := 1; i < len(diffStringArray); i++ {
		findIndex := strings.Index(diffStringArray[i], "@@") - 1
		diffStruct.File = p.Dir + diffStringArray[i][2:findIndex]
		changeRawArray := strings.Split(diffStringArray[i], "@@")
		for _, chunk := range changeRawArray {
			lineArray := strings.Split(chunk, "\n")
			for _, line := range lineArray {
				if len(line) < 1 {
					continue
				} else if strings.Index(line, "=") < 1 {
					continue
				} else if line[0:1] == "+" {
					line = strings.Replace(line, " ", "", -1)
					equalIndex := strings.Index(line, "=")
					valueChange.Key = line[1:equalIndex]
					valueChange.Val = strings.Replace(line[equalIndex+1:], ";", "", -1)
					diffStruct.Changes = append(diffStruct.Changes, valueChange)
					valueChange = valChange{}
				}
			}
		}
		c.Diffs = append(c.Diffs, diffStruct)
		diffStruct = diff{}
	}
	p.Commit.Diffs = c.Diffs
	return nil
}

func (p *Parser) GetGitHistory() error {
	files, _ := ioutil.ReadDir(p.Dir)
	for _, file := range files {
		if file.Name() == ".git" {
			p.GetCommits()
			var diffString string
			var err error
			diffString, err = p.GetDiffs(p.Commit.Hash)
			if err != nil {
				if err == io.EOF {
					break
				} else {
					return err
				}
			}
			var diffStruct []diff
			if diffString != "" {
				err := p.ParseDiffs(&p.Commit, diffString)
				if err != nil {
					return err
				}
			} else {
				p.Commit.Diffs = diffStruct
			}
		}
		return nil
	}
	return nil
}

func (p *Parser) CommitChange(filePath string) error {
	var addCmd *exec.Cmd
	var commitCmd *exec.Cmd
	addCmd = exec.Command("git", "add", filePath)
	commitCmd = exec.Command("git", "commit", "-m", "Changes made to: "+filePath)
	addCmd.Dir = p.Dir
	commitCmd.Dir = p.Dir
	_, err := addCmd.Output()
	if err != nil {
		fmt.Println("error adding changes", addCmd.Dir, addCmd.String())
		return err
	}
	_, err1 := commitCmd.Output()
	if err1 != nil {
		fmt.Println("error commiting changes", commitCmd.Dir, commitCmd.String())
		return err
	}
	return nil
}
